#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <stack>
#include <chrono>
#include <map>

using namespace std;

int solve();

int nVar, nClause;
vector<vector<int>> clauseList;

enum NodeType {
    root, implied
};
struct iGNode {
    bool value;
    int anteClause;
    int decLevel;
    NodeType ntype;
    vector<int> parents;
};
map<int, iGNode> impGraph;
int *varScore = NULL;
int currentDl = 0;
int lastAssignedVarNo = 0;

stack<int> unitClauses;
auto t1 = chrono::high_resolution_clock::now();

int read_input(char *fileName) {
    ifstream iFile(fileName);
    char temp[10];
    string str;

    // reading comments and parameters
    streampos oldpos;
    while (getline(iFile, str) && (str[0] == 'c' || str[0] == 'p')) {
        oldpos = iFile.tellg();
        if (str[0] == 'p') {
            string buf;
            stringstream ss(str);
            vector<string> tokens;
            while (ss >> buf)
                tokens.push_back(buf);
            nVar = stoi(tokens[tokens.size() - 2]);
            nClause = stoi(tokens[tokens.size() - 1]);
        }
    }

    // reading clauses
    iFile.seekg(oldpos);
    vector<int> singleClause;
    while (iFile >> temp) {
        // reading a clause with terminating 0
        if (temp[0] == '0') {
            if (singleClause.size() > 0) {
                clauseList.push_back(singleClause);
                singleClause.clear();
            }
        } else {
            int x = atoi(temp);
            singleClause.push_back(x);
        }
    }
    iFile.close();
    return 0;
}

int get_var(int x) {
    if (x < 0)
        return -x;
    else return x;
}

bool is_assigned(int var) {
    map<int, iGNode>::iterator itr = impGraph.find(var);
    if (itr == impGraph.end())
        return false;
    else
        return true;
}

bool get_assignment(int var) {
    map<int, iGNode>::iterator itr = impGraph.find(var);
    if (itr != impGraph.end())
        return itr->second.value;
}

bool is_unit(int clauseNo) {
    int unsatisfiedLit = 0;
    for (int j = 0; j < clauseList[clauseNo].size(); ++j) {
        if (is_assigned(get_var(clauseList[clauseNo][j]))) {
            map<int, iGNode>::iterator itr = impGraph.find(get_var(clauseList[clauseNo][j]));
            if (itr != impGraph.end()) {
                int litStatus;
                if (itr->second.value) {
                    litStatus = clauseList[clauseNo][j];
                } else {
                    litStatus = clauseList[clauseNo][j] * -1;
                }
                if (litStatus < 0)
                    unsatisfiedLit++;
                else
                    return false;
            }
        }
    }
    if ((clauseList[clauseNo].size() - unsatisfiedLit) == 1)
        return true;
    else
        return false;
}

bool is_unsatisfied(int clauseNo) {
    for (int i = 0; i < clauseList[clauseNo].size(); ++i) {
        if (is_assigned(get_var(clauseList[clauseNo][i]))) {
            map<int, iGNode>::iterator itr = impGraph.find(get_var(clauseList[clauseNo][i]));
            if (itr != impGraph.end()) {
                int litStatus;
                if (itr->second.value) {
                    litStatus = clauseList[clauseNo][i];
                } else {
                    litStatus = clauseList[clauseNo][i] * -1;
                }
                if (litStatus > 0)
                    return false;
            }
        } else {
            return false;
        }
    }
    return true;
}

int get_unassigned_lit(int clauseNo) {
    for (int j = 0; j < clauseList[clauseNo].size(); ++j) {
        if (!is_assigned(get_var(clauseList[clauseNo][j]))) {
            return clauseList[clauseNo][j];
        }
    }
    return 0;
}

int decide() {
    for (int i = 1; i < nVar + 1; ++i) {
        if (!is_assigned(i))
            return i;
    }
    return 0;
}

//detect conflict
int detect_conflict() {
    for (int i = 0; i < clauseList.size(); ++i) {
        if (is_unsatisfied(i)) {
            iGNode node;
            node.value = false;
            node.anteClause = i;
            node.decLevel = currentDl;
            node.ntype = implied;
            node.parents = vector<int> {};
            for (int j = 0; j < clauseList[i].size(); ++j) {
                node.parents.push_back(clauseList[i][j]);
            }
            impGraph.insert(pair<int, iGNode>(0, node));

            return -1;
        }
    }
    return 0;
}


void check_and_update_lit_score() {
    if ((impGraph.size() - lastAssignedVarNo) >= 100) {
        for (int i = 1; i <= nVar * 2; ++i) {
            varScore[i] = varScore[i] / 2;
        }
        lastAssignedVarNo = impGraph.size();
    }
}

void update_unit_clause_list() {
    for (int i = 0; i < clauseList.size(); ++i) {
        if (is_unit(i)) {
            unitClauses.push(i);
        }
    }
}

// returns -1 for conflict otherwise 0
// node 0 represents a conflict node
int bcp() {
    update_unit_clause_list();
    check_and_update_lit_score();
    while (!unitClauses.empty()) {
        int i = unitClauses.top();
        unitClauses.pop();
        int l = get_unassigned_lit(i);

        iGNode node;
        if (l < 0)
            node.value = false;
        else
            node.value = true;
        node.anteClause = i;
        node.decLevel = currentDl;
        node.ntype = implied;
        node.parents = vector<int> {};
        for (int j = 0; j < clauseList[i].size(); ++j) {
            if (clauseList[i][j] != l)
                node.parents.push_back(clauseList[i][j]);
        }
        impGraph.insert(pair<int, iGNode>(get_var(l), node));

        if (detect_conflict() == -1) {
            while (!unitClauses.empty())
                unitClauses.pop();
            return -1;
        }
        update_unit_clause_list();
    }
    return 0;
}


void construct_var_score() {
    for (int i = 0; i < clauseList.size(); ++i) {
        for (int j = 0; j < clauseList[i].size(); ++j) {
            if (clauseList[i][j] < 0) {
                varScore[(clauseList[i][j] * -1) + nVar]++;
            } else {
                varScore[clauseList[i][j]]++;
            }
        }
    }
}

int get_high_score_literal() {
    int high = 1;
    int highScore = 0;
    for (int i = 1; i <= nVar * 2; ++i) {
        if (varScore[i] >= highScore) {
            int var = i;
            if (i > nVar)
                var = i - nVar;
            if (!is_assigned(var)) {
                high = i;
                highScore = varScore[i];
            }
        }
    }
    if (high > nVar)
        return (high - nVar) * -1;
    else
        return high;
}

void make_decision(int decisionLit) {

    iGNode node;
    if (decisionLit < 0)
        node.value = false;
    else
        node.value = true;
    node.anteClause = -1;
    node.decLevel = currentDl;
    node.ntype = root;
    node.parents = vector<int> {};
    impGraph.insert(pair<int, iGNode>(get_var(decisionLit), node));
}

void build_conflict_Clause() {
    vector<int> conflictClause;

    map<int, iGNode>::iterator itr = impGraph.find(get_var(0));
    for (int k = 0; k < itr->second.parents.size(); ++k) {
        map<int, iGNode>::iterator itr2 = impGraph.find(get_var(itr->second.parents[k]));
        if (itr2->second.decLevel != currentDl) {
            if (get_assignment(get_var(itr->second.parents[k]))) {
                conflictClause.push_back(get_var(itr->second.parents[k]) * -1);
            } else {
                conflictClause.push_back(get_var(itr->second.parents[k]));
            }
        }
    }

    for (pair<int, iGNode> element : impGraph) {
        if (element.second.decLevel == currentDl && element.first != 0) {
            if (element.second.ntype == root) {
                if (get_assignment(element.first)) {
                    vector<int>::iterator it;
                    it = find(conflictClause.begin(), conflictClause.end(), (element.first * -1));
                    if (it == conflictClause.end())
                        conflictClause.push_back(element.first * -1);

                } else {
                    vector<int>::iterator it;
                    it = find(conflictClause.begin(), conflictClause.end(), (element.first));
                    if (it == conflictClause.end())
                        conflictClause.push_back(element.first);
                }

            } else {
                for (int i = 0; i < element.second.parents.size(); ++i) {
                    map<int, iGNode>::iterator itrParents = impGraph.find(get_var(element.second.parents[i]));
                    if (itrParents->second.decLevel != currentDl) {
                        if (get_assignment(get_var(element.second.parents[i]))) {
                            vector<int>::iterator it;
                            it = find(conflictClause.begin(), conflictClause.end(),
                                      (get_var(element.second.parents[i]) * -1));
                            if (it == conflictClause.end())
                                conflictClause.push_back(get_var(element.second.parents[i]) * -1);
                        } else {
                            vector<int>::iterator it;
                            it = find(conflictClause.begin(), conflictClause.end(),
                                      (get_var(element.second.parents[i])));
                            if (it == conflictClause.end())
                                conflictClause.push_back(get_var(element.second.parents[i]));
                        }
                    }
                }
            }
        }
    }

    // add score of the literals
    for (int j = 0; j < conflictClause.size(); ++j) {
        if (conflictClause[j] < 0) {
            varScore[(conflictClause[j] * -1) + nVar]++;
        } else {
            varScore[conflictClause[j]]++;
        }
    }

    // add the clause to clause list
    clauseList.push_back(conflictClause);
}

int decide_backtrack_level() {
    int backtrackLevel = -1;
    int ccNo = clauseList.size() - 1;

    if (currentDl == 0)
        return backtrackLevel;
    else if (clauseList[ccNo].size() == 1)
        return 0;
    else {
        for (int i = 0; i < clauseList[ccNo].size(); ++i) {
            map<int, iGNode>::iterator itr = impGraph.find(get_var(clauseList[ccNo][i]));
            if (itr != impGraph.end()) {
                if ((itr->second.decLevel > backtrackLevel) && (itr->second.decLevel < currentDl))
                    backtrackLevel = itr->second.decLevel;
            }
        }
        return backtrackLevel;
    }
}

int analyze_conflict() {
    build_conflict_Clause();
    return decide_backtrack_level();
}

void backtrack(int backtrackLevel) {
    vector<int> toBeDeleted = {};
    for (pair<int, iGNode> element : impGraph) {
        if (element.second.decLevel > backtrackLevel) {
            toBeDeleted.push_back(element.first);
        }
    }

    for (int i = 0; i < toBeDeleted.size(); ++i) {
        map<int, iGNode>::iterator itr = impGraph.find(toBeDeleted[i]);
        impGraph.erase(itr);
    }
    toBeDeleted.clear();
    currentDl = backtrackLevel;
}

void print_result(bool result) {
    auto t2 = chrono::high_resolution_clock::now();
    chrono::duration<double, milli> elapsed = t2 - t1;
    cout << "c Given formula has " << nVar << " variables and " << nClause << " clauses." << endl;
    if (result) {
        cout << "s SATISFIABLE" << endl;
        cout << "v ";
        for (pair<int, iGNode> element : impGraph) {
            if (element.first != 0) {
                if (element.second.value) {
                    cout << element.first << " ";
                } else {
                    cout << element.first * -1 << " ";
                }
            }
        }
        cout << "0" << endl;
    } else {
        cout << "s UNSATISFIABLE" << endl;
    }
    cout << "c Done (mycputime is " << elapsed.count() / 1000 << "s)." << endl;
}

int solve() {
    impGraph.clear();
    if (bcp() == -1) {
        print_result(false);
        return 0;
    }
    while (true) {
        int decisionVar = decide();
        if (decisionVar == 0) {
            print_result(true);
            return 0;
        } else {
            currentDl++;
            make_decision(get_high_score_literal());
            while (bcp() == -1) {
                int backtrackLevel = analyze_conflict();
                if (backtrackLevel < 0) {
                    print_result(false);
                    return 0;
                } else {
                    backtrack(backtrackLevel);
                    if (currentDl == 0)
                        break;
                }
            }
        }
        if (currentDl == 0)
            break;
    }
    return -1;
}

int main(int argc, char *argv[]) {

    // getting command line argument
    char fileName[100];
    if (argc == 2) {
        strncpy(fileName, argv[1], 100);
    } else {
        cout << "Provide required number of arguments correctly." << endl;
    }

    if (read_input(fileName) == -1) {
        return 0;
    }

    varScore = new int[nVar * 2 + 1]{0};
    construct_var_score();

    while (solve() == -1);

    delete varScore;
    varScore = NULL;
    return 0;
}